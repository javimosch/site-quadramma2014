/*-----------------------------------------------------------------------------------*/
/*  PRELOADER
/*-----------------------------------------------------------------------------------*/
//jQuery(window).load(function() {
	//Preloader
	setTimeout("jQuery('#preloader').animate({'opacity' : '0'},300,function(){jQuery('#preloader').hide()})", 400);
	setTimeout("jQuery('.preloader_hide, .selector_open').animate({'opacity' : '1'},500)", 3000);
	setTimeout("jQuery('footer').animate({'opacity' : '1'},500)", 3000);

//});



function bindWorkProjects() {
	console.log("bindWorkProjects");
	$(".img-jazmin-big").click(function() {
		console.log("CLICK");
		$.fancybox.open({
			href: '#jazminchebar',
			padding: 0,
			margin: 0,
			width: "100%",
			height: "70%",
			titleShow: false,
			transitionIn: 'elastic',
			transitionOut: 'elastic',
			fitToView: true,
			closeBtn: true,
			//scrolling: "no"
		});
		$('#slides').slidesjs({
			width: 940,
			height: 528,
			navigation: false
		});
	});
}



$(function() {
	//--------------------------------------------
	var $workBigItems = [];
	//--------------------------------------------
	$(document).ready(function() {

		//$(".fancybox").fancybox();
		/*
		$(".fancybox").fancybox({
			beforeShow: function() {
				$("body *:not(.fancybox-overlay, .fancybox-overlay *)").addClass("blur");
			},
			afterClose: function() {
				$("body *:not(.fancybox-overlay, .fancybox-overlay *)").removeClass("blur");
			}
		});
*/

		//bindWorkProjects();

		//--------------------------------------------
		//console.log("ready");
		$(".work-big ul li").each(function() {
			//$workBigItems.push($(this));
		});
		if ($(window).width() > 1024) {
			//$('#section-1').parallax("50%", 0.2);
			//$('#section-3').parallax("50%", -0.5);
			//$('#section7image').parallax("50%", 0.5);
		}
		//--------------------------------------------
		function onresize() {
			//--------------------------------------------
			//adjustWorkItems();
			//--------------------------------------------
		}

		function adjustWorkItems() {
			var w = $(window).width();
			if (w <= 480) {
				detachAll(".work-big ul li");
				attachItems(".work-big ul", $workBigItems, 4);
				return;
			}
			if (w <= 600) {
				detachAll(".work-big ul li");
				attachItems(".work-big ul", $workBigItems, 2);
				return;
			}
			if (w < 1024) {
				detachAll(".work-big ul li");
				attachItems(".work-big ul", $workBigItems, 3);
				return;
			}
			if (w >= 1024) {
				detachAll(".work-big ul li");
				attachItems(".work-big ul", $workBigItems, 4);
				return;
			}
		}

		function attachItems(selector, items, size) {
			if (items.length < size)
				size = items.length;
			for (var x = 0; x < size; x++) {
				$(selector).append(items[x]);
			}
		}

		function detachAll(selection) {
			$(selection).each(function() {
				$(this).detach();
			});
		}

		function onscroll() {}

		function hideLines(arr) {
			_.each(arr, function($line, index) {
				$line.tween({
					width: {
						start: $line.width(),
						stop: 0,
						time: 0,
						duration: 0.4,
						units: 'px',
						effect: 'easeInOut'
					}
				}).play();

			});
		}

		function showSelected($selector) {
			$linegroup = $selector.children(".line-group").contents();
			$linegroup.each(function() {
				if ($(this).attr("class") != "line-hide") {
					var $selected = $(this);
					$selected.tween({
						width: {
							start: 0,
							stop: 241,
							time: 0,
							duration: 0.4,
							units: 'px',
							effect: 'easeInOut'
						}
					}).play();
				}
			});
		}

		var $lines = []; //LINES
		$(".work ul li,.work-big ul li").children(".line-group").contents().each(function() {
			if ($(this).attr("class") != "line-hide") {
				$lines.push($(this));
			}
		});;
		hideLines($lines); //HIDE AT BEGIN
		$(".work ul li,.work-big ul li").bind("mouseenter", function() { //event
			hideLines($lines);
			//console.log("hide all");
			showSelected($(this));
			//console.log("show selected");
		});
		/*
		$(".work ul li,.work-big ul li").bind("mouseout", function() { //event
			hideLines($lines);
		});
*/

		//Smooth Scrolling

		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
		$(window).resize(onresize);
		$(window).scroll(onscroll);
		onresize();
		//--------------------------------------------
	});
	//
});