angular.module("QAppGeneralModule", [
	"QWorkModule"
])

.controller("QPageController",

	function($scope) {

		console.info("[QPageController]");

		var lastY;
		$(document).bind('touchmove', function(e) {
			var currentY = e.touches[0].clientY;
			if (currentY !== lastY) {
				return;
			}
			lastY = currentY;
		});

		$('.navbar-collapse a').click(function(e) {
			$('.navbar-collapse').collapse('toggle');
		});

		function parallaxStuff() {
			if ($(window).width() > 1200) {
				//$('#home').parallax("50%", 0.8);
				//$('#video').parallax("50%", 0.1);
				//$('#section-7-img').parallax("50%", 0.3);
				var s = skrollr.init({
					smoothScrolling: true,
					forceHeight: true,
				});
			}
		}
		$(window).resize(parallaxStuff);
		parallaxStuff();


		console.info("[QPageController] - ScrollTo -> " + _php.scrollTo);

		$scope.pushState = function(name) {
			window.history.pushState({}, name, name);
			$("title").html("Quadramma" + " | " + name);
		}


		function scrollTo(domID) {
			var target = $("#" + domID);
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}

		Modalbehavours();

		//default
		scrollTo(_php.scrollTo);
		$scope.pushState(_php.scrollTo);

	}
);


function Modalbehavours() {
	/* blur on modal open, unblur on close */
	$('.modal').on('show.bs.modal', function() {
		$('.container').addClass('blur');
	})

	$('.modal').on('hide.bs.modal', function() {
		$('.container').removeClass('blur');
	})
}