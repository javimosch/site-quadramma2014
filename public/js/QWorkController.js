/*

function QJSlider(name, slides) {
    //console.info(slides);
    var _slide = _.find(slides, function(s) {
        return s.name == name;
    });
    //
    var modalSelector = "#workModal-" + _slide.name;
    var $modal = $(modalSelector);
    var $ul = $modal.find("ul");
    var ulWidth = $modal.width();
    // console.info(modalSelector);
    //console.info(ulWidth);

    imgWidth = $modal.find("li img").width();
    $("div.qjslider").width(ulWidth);
    //$modal.find("li img").width(ulWidth);
    //$modal.find("li img").css({opacity:0});

    //------------------------------------------------------
    var index = 0;
    //------------------------------------------------------
    return {
        next: function() {
            console.info("QJSlider-> next");
            index++;
            if (index > 2) index = 0;
            var left = (ulWidth * index) * -1;
            console.info(left);
            $ul.animate({
                left: left
            }, "slow");
        },
        prev: function() {
            console.info("QJSlider-> prev");
            index--;
            if (index < 0) index = 2;
            var left = (ulWidth * index) * -1;
            console.info(left);
            $ul.animate({
                left: left
            }, "slow");
        }
    };
}

*/



angular.module("QWorkModule", []).controller("QWorkController", function($scope, $timeout) {
    console.info("[QWorkController]");

    function initialize() {
        var json = $.getJSON("work.json", function(json) {
            $scope.workSlides = json;
            updateSlidesItems();
            setTimeout(function() {
                $scope.$apply(function() {

                });
            }, 1000);
        });
    }

    function updateSlidesItems() {
        console.info("-updateSlidesItems-");
        _.each($scope.workSlides, function(slide) {
            var mobileSufix = "_mobile";
            if ($(window).width() > 768) {
                mobileSufix = "";
            }
            //console.info("requesting " + slide.name + mobileSufix);
            slide.items = _.sortBy(_php[slide.name + mobileSufix], function(val) {
                return val;
            });
            if (_.isUndefined(slide.items)) {
                console.log(slide.name + mobileSufix + ": carpeta slides vacia!");
                slide.items = [];
            }
        });
    }

    $scope.modal = function(name) {
        $('#' + "workModal-" + name).modal('show');
        var slide = _.find($scope.workSlides, function(s) {
            return s.name == name;
        });



        var $telon = $("<div/>").css({
            "width": "100%",
            "height": "100%",
            "z-index": "999999",
            "background-color": "rgba(0,0,0,1)",
            "position": "absolute"
        });
        $('#' + "workModal-" + name).prepend($telon);


        setTimeout(function() {
            $('#work-slide-' + name).bxSlider({
                auto: true,
                autoControls: false,
                speed: 500
            });

            setTimeout(function() {

                $controls = $(".bx-controls-direction").detach();
                $('#' + "workModal-" + name).prepend($controls);

                $telon.animate({
                    "background-color": "rgba(0,0,0,0)"
                }, 500, function() {
                    $(".bx-controls-direction a").css({display:"block"});
                    $telon.remove();
                });
            }, 200);

        }, 700);


    };


    //INIT
    initialize();
    $(window).resize(updateSlidesItems)

    /*

  $scope.classFor = function(slide, $index) {
        var responsiveClass = "";
        return slide.active === $index.toString() ? "active " + responsiveClass : responsiveClass;
    };
*/

    /*
    $scope.selectSlideImage = function(slide, $index) {
        // slide.active = $index.toString();
    };
    */
    /*
    $scope.classForBoxTitle = function(slide, $index) {
        return "text color digital";
    }
    $scope.classForBox = function(slide, $index) {
        var active = $index.toString() === slide.active;
        return "box " + (active ? "active" : "");
    };
*/
    //var _currSlider = null;


    /*
    function getActive(slide, direction) {
        var active = parseInt(slide.active);
        switch (active) {
            case 0:
                active = (direction == 1) ? 2 : 1;
                break;
            case 2:
                active = (direction == 1) ? 1 : 0;
                break;
            case 1:
                active = (direction == 1) ? 0 : 2;
                break;
        }
        return active;
    }
    */
    /*
    function advance(slideName, active) {
        $timeout(function() {
            $scope.$apply(function() {
                $scope.selectSlideImage(_.find($scope.workSlides, function(s) {
                    return s.name == slideName;
                }), active);
            });
        }, 200);
    }
    */
    /*
    $scope.next = function(slide) {
        //var active = getActive(slide, 1);
        //advance(slide.name, active);
        _currSlider.next();
    }
    $scope.prev = function(slide) {
        //var active = getActive(slide, 1);
        //advance(slide.name, active);
        _currSlider.prev();
    }
*/

    /*
    var _currentAutoSlideInterval = null;

    function autoSlide(name) {

        var _slide = _.find($scope.workSlides, function(s) {
            return s.name == name;
        });

        if (_currentAutoSlideInterval != null) {
            clearInterval(_currentAutoSlideInterval);
        }
        _currentAutoSlideInterval = setInterval(function() {


            var active = parseInt(_slide.active);

            //console.info("got active -> "+active);

            switch (active) {
                case 0:
                    active = 2;
                    break;
                case 2:
                    active = 1;
                    break;
                case 1:
                    active = 0;
                    break;
            }

            //console.info("changing to active -> "+active);

            $timeout(function() {
                $scope.$apply(function() {
                    $scope.selectSlideImage(_.find($scope.workSlides, function(s) {
                        return s.name == name;
                    }), active);
                });
            }, 200);



            //  console.info("Interval ->  " + _slide.name + " to " + active);

        }, 5000);
    }
    */


});