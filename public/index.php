<?php
$root = "/site2014";

require 'flight/Flight.php';  



Flight::route('/', function(){ //ROOT
    $scrollTo = "home";
    include "views/layout.site.php";
});

Flight::route('/home', function(){
    $scrollTo = "home";
    include "views/layout.site.php";
});
Flight::route('/about', function(){
    $scrollTo = "about";
    include "views/layout.site.php";
});
Flight::route('/work', function(){
    $scrollTo = "work";
    include "views/layout.site.php";
});
Flight::route('/services', function(){
    $scrollTo = "services";
    include "views/layout.site.php";
});
Flight::route('/video', function(){
    $scrollTo = "video";
    include "views/layout.site.php";
});
Flight::route('/team', function(){
    $scrollTo = "team";
    include "views/layout.site.php";
});
Flight::route('/contact', function(){
    $scrollTo = "contact";
    include "views/layout.site.php";
});





/*

Flight::map('error', function(Exception $ex){
    echo $ex->getTraceAsString();
});


Flight::set('flight.log_errors', true);
Flight::map('notFound', function(){
    Flight::redirect("/");
});

*/

Flight::start();
?>
