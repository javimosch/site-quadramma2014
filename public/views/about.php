<div id="about" class="section container-fluid">
    
    <div class="section row-fluid">
        <div class="span12">
            
            <div class="section title">Connecting people<br>beyond digital.</div>
            
            <div class="section arrow"><img src="img/arrow_down.png" alt="Arrow" /></div>
            
            <p class="text">Quadramma is about creating real-time customer experiences, moving beyond digital<br>
            and connecting people with the world.</p>

        </div>
    </div>
    
</div>