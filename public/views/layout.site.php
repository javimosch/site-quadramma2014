<!DOCTYPE html>
<html ng-app="QApp" ng-controller="QPageController">
<?php include "views/head.php" ?>
<body>
    <!-- PRELOAD Y HEADER ------------------------------------------ -->
    <img id="preloader" src="img/preloader.gif" alt="" />
    <div class="preloader_hide">
        <div id="page" data-smooth-scrolling="on">
            <div class="container-fluid">
                <div class="row-fluid">
                    
                        <?php include "views/menu.php" ?>
                        <?php include "views/modals.php" ?>
                        <?php include "views/home.php" ?>
                        <?php include "views/about.php" ?>
                        <?php include "views/work.php" ?>
                        <?php include "views/services.php" ?>
                        <?php include "views/video.php" ?>
                        <?php include "views/team.php" ?>
                        <?php include "views/contact.php" ?>
                    
                </div>
            </div>
        </div>
        <!-- FOOTER -->
        <footer>
            
        </footer>
    </div>
    <!-- PRELOAD Y HEADER  END ------------------- -->
</body>
</html>