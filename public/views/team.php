<div id="team" class="section container-fluid">
        <div class="section row-fluid">
            <div class="span12">
                
                
                
                <div id="team-team"  class="section title">Meet The Quad</div>
                <div class="section arrow"><img class="team arrow img" src="img/arrow_down_2.png" alt="Arrow" /></div>
                
                <div class="qm team container">
                    <ul class="qm team">
                        <li class="qm team item">
                            <div class="qm team item image dami"></div>
                            <div class="qm team item title">
                                Damian Aliberti
                            </div>
                            <div class="qm team item role">
                                BUSINESS DIRECTOR<br>& CO-FOUNDER
                            </div>
                            <div class="qm team item separator"></div>
                            <div class="qm team item about">
                                His strategic vision empowers<br>
                                "the Quad" creating and<br>
                                inspiring a collaborative<br>
                                atmosphere to foster unique<br>
                                business opportunities.
                            </div>
                            <div class="qm team item email">
                                
                            </div>
                        </li>
                        <li class="qm team item">
                            <div class="qm team item image ari"></div>
                            <div class="qm team item title">
                                Ariel Chamson
                            </div>
                            <div class="qm team item role">
                                CREATIVE DIRECTOR<br>& CO-FOUNDER
                            </div>
                            <div class="qm team item separator"></div>
                            <div class="qm team item about">
                                He is the multi-talented<br>
                                creative force behind the Quad.<br>
                                From concept to execution,<br>
                                he brings a unique perspective<br>
                                to the gaming and mobile industry
                            </div>
                            <div class="qm team item email">
                                
                            </div>
                        </li>
                         <li class="qm team item">
                            <div class="qm team item image maru"></div>
                            <div class="qm team item title">
                                Maru Eskenazi
                            </div>
                            <div class="qm team item role">
                                EXECUTIVE PRODUCER
                            </div>
                            <div class="qm team item separator"></div>
                            <div class="qm team item about">
                                Her knowledge and organizational<br>
                                skills are key to every single<br>
                                project, achieving success<br>
                                is what she does better.
                            </div>
                            <div class="qm team item email">
                                
                            </div>
                        </li>
                         <li class="qm team item">
                            <div class="qm team item image axel"></div>
                            <div class="qm team item title">
                                Axel Chamson
                            </div>
                            <div class="qm team item role">
                                BUSINESS DEVELOPMENT<br>BRAZIL
                            </div>
                            <div class="qm team item separator"></div>
                            <div class="qm team item about">
                                He's the ace. A strategic<br>
                                mind with a collaborative<br>
                                vision with a hightrack record<br>
                                of winning market share and<br>
                                developing business strategies
                            </div>
                            <div class="qm team item email">
                                
                            </div>
                        </li>
                         <li class="qm team item">
                            <div class="qm team item image javi"></div>
                            <div class="qm team item title">
                                Javi Arancibia
                            </div>
                            <div class="qm team item role">
                                TECHNOLOGY LEAD<br>DEVELOPER
                            </div>
                            <div class="qm team item separator"></div>
                            <div class="qm team item about">
                                His excellent understanding<br>
                                of the technology combined with<br>
                                the knowledge of informational<br>
                                systems makes him a key member<br>
                                of the Quad.
                            </div>
                            <div class="qm team item email">
                                
                            </div>
                        </li>
                         <li class="qm team item">
                            <div class="qm team item image buba"></div>
                            <div class="qm team item title">
                                Gabriel Vion
                            </div>
                            <div class="qm team item role">
                                INTERACTIVE & MOBILE<br>DEVELOPER
                            </div>
                            <div class="qm team item separator"></div>
                            <div class="qm team item about">
                                A young mind with an experienced<br>
                                and skilled background in tech<br>
                                development for web, apps<br>
                                and mobile, oriented on user<br>
                                experience and performance.
                            </div>
                            <div class="qm team item email">
                                
                            </div>
                        </li>
                         <li class="qm team item">
                            <div class="qm team item image santi"></div>
                            <div class="qm team item title">
                                Santi Navatta
                            </div>
                            <div class="qm team item role">
                                UX & IA DESIGNER
                            </div>
                            <div class="qm team item separator"></div>
                            <div class="qm team item about">
                                A talented artist with a unique<br>
                                eye for design and technology,<br>
                                producing unique and engaging<br>
                                experiences with his creative<br>
                                mind and ideas.
                            </div>
                            <div class="qm team item email">
                                
                            </div>
                        </li>
                         <li class="qm team item">
                            <div class="qm team item image pet"></div>
                            <div class="qm team item title">
                                Scratch
                            </div>
                            <div class="qm team item role">
                                OFFICE PET
                            </div>
                            <div class="qm team item separator"></div>
                            <div class="qm team item about">
                                He is driven, risky, and with<br>
                                a creative and daring personality.<br>
                                A true inspiration to "The Quad",<br>
                                plus he's nuts, like us.
                            </div>
                            <div class="qm team item email">
                                
                            </div>
                        </li>
                    </ul>
                </div>

    
            </div>
        </div>
    </div>