<div id="home" class="section container-fluid">
	<div class="section row-fluid">

        <ul class="cb-slideshow">
            <li><span>Image 01</span></li>
            <li><span>Image 02</span></li>
            <li><span>Image 03</span></li>
        </ul>         

		<div class="home text span12 align-center text-center">
			
			<div class="home date">
				<img class="img-responsive" src="img/home_date.png" alt="2014" />
			</div>

			<div class="logo" data-0="margin-top:0px;"     data-250="margin-top:-50px;">
				
				<img class="img-responsive" src="img/home_logo.png" alt="Quadramma" />
				
			</div>

			<div class="scrolldown">        
				<img class="img-responsive" src="img/home_explore.png" alt="Scroll down" />      
				<a href="#work"  ng-click="pushState('work');">
					<div class="button explore"></div>
				</a>
			 </div>
		</div>

	</div>
</div>