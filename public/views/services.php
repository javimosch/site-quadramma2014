<div id="services" class="section container-fluid">
    <div class="section row-fluid">
        <div class="span12">
            <div class="section title">360º Experience<br>within and beyond.</div>
            <div class="section arrow"><img class="services arrow img" src="img/arrow_down_2.png" alt="Arrow" /></div>
            
            <ul>
                <li>
                    <div class="subtitle"><p>DIGITAL</p></div>
                    <div class="line-digital"></div>
                    <div class="text"><p>Web Apps<br>
                        Mobile Apps<br>
                        iOS/Android<br>
                        Responsive Design<br>
                        Augmented Reality <br>
                        Motion Control
                        
                        
                    </p></div>
                </li>
                <li>
                    <div class="subtitle"><p>BRAND</p></div>
                    <div class="line-brand"></div>
                    <div class="text"><p>
                        Branding,<br>
                        Business Strategy<br>
                        Social Media<br>
                        Advertising<br>
                        Content creation<br>
                        Live experiences
                    </p></div>
                </li>
                <li>
                    <div class="subtitle"><p>GAMES</p></div>
                    <div class="line-games"></div>
                    <div class="text"><p>
                        Casual games<br>
                        Desktop & Mobile<br>
                        Online games<br>
                        Advergames<br>
                        IP creation<br>
                        Smart toys<br>
                    </p></div>
                </li>
                
            </ul>
        </div>
    </div>
</div>