<nav id="menu" class="navbar-fixed-top navbar-default" role="navigation">
        
        <ul class="nav navbar-nav navbar-right">
            <li class="">
                <div class="social-container">
                    <a target="_blank" href="https://www.facebook.com/Quadramma"> <div class="social social-facebook">    </div></a>
                    <a target="_blank" href="https://twitter.com/quadramma">      <div class="social social-twitter">     </div></a>
                    <!--
                    <a target="_blank" href="https://plus.google.com">   <div class="social social-google">      </div></a>
                    -->
                </div>
            </li>
        </ul>
        
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
            <span class="sr-only">Desplegar navegación</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <!-- <a class="navbar-brand" href="#">Logotipo</a> -->
        </div>
        
        <!-- Agrupar los enlaces de navegación, los formularios y cualquier
        otro elemento que se pueda ocultar al minimizar la barra -->
        <div id="qm-menu" class="collapse navbar-collapse navbar-ex1-collapse" >
            <ul class="sections nav navbar-nav">
                <li class=""><a href="#home"  ng-click="pushState('home');">Home</a></li>
                <li class=""><a href="#work"  ng-click="pushState('work');">Work</a></li>
                <li class=""><a href="#services"  ng-click="pushState('services');">Services</a></li>
                <!--
                <li class=""><a href="#video"  ng-click="pushState('video');">Video</a></li>
                -->
                <li class=""><a href="#about"  ng-click="pushState('about');">About</a></li>
                <li class=""><a href="#team"  ng-click="pushState('team');">Team</a></li>
                <li class=""><a href="#contact"    ng-click="pushState('contact');">Contact</a></li>
            </ul>
        </div>
        
        
    </nav>