<head>
    <!-- *********** ---------- ************ -->
    <title>Quadramma</title>
    <meta name="description" content="">
    <meta name="keywords" content="web design games">
    <meta name="author" content="Quadramma">
    <!-- *********** ---------- ************ -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cleartype" content="on">
    <link rel="shortcut icon" href="/favicon.ico">
    <!-- *********** ---------- ************ -->
    <!--
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    
    <link href="css/quadramma.css" rel="stylesheet" type="text/css" />
    <link href="css/quadramma.video.css" rel="stylesheet" type="text/css" />
    <link href="css/quadramma.team.css" rel="stylesheet" type="text/css" />
    <link href="css/quadramma.slide.css" rel="stylesheet" type="text/css" />
    <link href="css/quadramma.home.slide.css" rel="stylesheet" type="text/css" />

    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    

    <link href='http://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Syncopate' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- *********** ---------- ************ -->
    <script src="lib/jquery-2.0.3.min.js"></script>
    <!--
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js"></script>
    -->
    <script src="lib/angular.min.js"></script>
    <script type="text/javascript" src="lib/underscore-min.js"></script>
    <!--
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    -->
    <script type="text/javascript" src="lib/bootstrap.min.js"></script>
    <!-- *********** ---------- ************ -->
    <script type="text/javascript" src="lib/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="lib/jstween-1.1.min.js"></script>
    <!--
    <script type="text/javascript" src="lib/jquery.fancybox.js?v=2.1.5"></script>
    -->

    
    <script type="text/javascript" src="lib/jquery.bxslider.min.js"></script>

    <script type="text/javascript" src="lib/jquery.slides.min.js"></script>
    <script type="text/javascript" src="lib/skrollr.min.js"></script>
    <!-- *********** ---------- ************ -->
    <script type="text/javascript" src="js/QApp.js"></script>
    <script type="text/javascript" src="js/QPageController.js"></script>
    <script type="text/javascript" src="js/QWorkController.js"></script>




    <script type="text/javascript" src="js/main.js"></script>
    <!-- ****************************** -->

    <script>


    <?php 
        function GetFiles($path){
          $rta = array();
          if ($handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    if (strpos($entry,'.') !== false) {
                        $rta [] = $path . "/" . $entry;
                    }else{
                        
                    }
                }
            }
            closedir($handle);
          }
          return $rta;
        }
        
        $starbucks = GetFiles("resources/starbucks/slides");
        $lego = GetFiles("resources/lego/slides");
        $tntoscars = GetFiles("resources/tntoscars/slides");
        $jazmin = GetFiles("resources/jazmin/slides");

        $artplacer = GetFiles("resources/artplacer/slides");
        $orbians = GetFiles("resources/orbians/slides");
        $kgd = GetFiles("resources/kgd/slides");
        $misspies = GetFiles("resources/misspies/slides");

        $dianavinoli = GetFiles("resources/dianavinoli/slides");
        $cosmeticshield = GetFiles("resources/cosmeticshield/slides");
        $personal = GetFiles("resources/personal/slides");
        $bizonty = GetFiles("resources/bizonty/slides");

        $rafaelvinoli = GetFiles("resources/rafaelvinoli/slides");
        $tntplaytime = GetFiles("resources/tntplaytime/slides");
        $axn = GetFiles("resources/axn/slides");
        $esfera = GetFiles("resources/esfera/slides");
    ?>

        _php = {};
        _php.scrollTo = "<?=$scrollTo?>";
        
        _php.starbucks  = JSON.parse('<?=json_encode($starbucks)?>');
        _php.starbucks_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/starbucks/slides_mobile"))?>');

        _php.lego  = JSON.parse('<?=json_encode($lego)?>');
        _php.lego_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/lego/slides_mobile"))?>');

        _php.tntoscars  = JSON.parse('<?=json_encode($tntoscars)?>');
        _php.tntoscars_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/tntoscars/slides_mobile"))?>');

        _php.jazmin  = JSON.parse('<?=json_encode($jazmin)?>');
        _php.jazmin_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/jazmin/slides_mobile"))?>');

        _php.artplacer  = JSON.parse('<?=json_encode($artplacer)?>');
        _php.artplacer_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/artplacer/slides_mobile"))?>');

        _php.orbians  = JSON.parse('<?=json_encode($artplacer)?>');
        _php.orbians_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/orbians/slides_mobile"))?>');

        _php.kgd  = JSON.parse('<?=json_encode($kgd)?>');
        _php.kgd_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/kgd/slides_mobile"))?>');

        _php.misspies  = JSON.parse('<?=json_encode($misspies)?>');
        _php.misspies_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/misspies/slides_mobile"))?>');

        _php.dianavinoli  = JSON.parse('<?=json_encode($dianavinoli)?>');
        _php.dianavinoli_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/dianavinoli/slides_mobile"))?>');

        _php.cosmeticshield  = JSON.parse('<?=json_encode($cosmeticshield)?>');
        _php.cosmeticshield_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/cosmeticshield/slides_mobile"))?>');

        _php.personal  = JSON.parse('<?=json_encode($personal)?>');
        _php.personal_mobilke  = JSON.parse('<?=json_encode(GetFiles("resources/personal/slides_mobile"))?>');

        _php.bizonty  = JSON.parse('<?=json_encode($bizonty)?>');
        _php.bizonty_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/bizonty/slides_mobile"))?>');
        
        _php.rafaelvinoli  = JSON.parse('<?=json_encode($rafaelvinoli)?>');
        _php.rafaelvinoli_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/rafaelvinoli/slides_mobile"))?>');

        _php.tntplaytime  = JSON.parse('<?=json_encode($tntplaytime)?>');
        _php.tntplaytime_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/tntplaytime/slides_mobile"))?>');

        _php.axn  = JSON.parse('<?=json_encode($axn)?>');
        _php.axn_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/axn/slides_mobile"))?>');

        _php.esfera  = JSON.parse('<?=json_encode($esfera)?>');
        _php.esfera_mobile  = JSON.parse('<?=json_encode(GetFiles("resources/esfera/slides_mobile"))?>');
    </script>

</head>
