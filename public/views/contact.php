<div id="section-7-img" class="section container-fluid">
    <div class="section row-fluid">
        <div class="span12">
        </div>
    </div>
</div>
<div id="section-7" class="section container-fluid">
    <div id="contact" class="row-fluid">
        <div class="span12">
            
            <ul id="">
                <li class="quadramma">
                    <div>
                        <img src="img/logo_mini.png" alt="Quadramma"/>
                    </div>    
                    <div class="title"><p>BUENOS AIRES, ARGENTINA</p></div>
                    <div class="text"><p>+54 11 2070 8326<br>Olleros 3570 5B, CABA</p></div>
                    <div class="title"><p>RIO DE JANEIRO, BRAZIL</p></div>
                    <div class="text"><p>+55 21 9558 6065<br>Av. Rui Barbosa 636 / 1504</p></div>
                    <div class="title"><p>ATLANTA, USA</p></div>
                    <div class="text"><p>404 890 8093</p></div>
                </li>

                <li class="drop-a-line">
                    <img src="img/drop_a_line.png" alt="Quadramma"/>
                    <div class="title"><p>GET IN TOUCH</p></div>
                    <div class="text"><p>office@quadramma.com</p></div>
                    <div class="title"><p>JOIN US</p></div>
                    <div class="text"><p>job@quadramma.com</p></div>
                </li>
                <li class="visit-our-offices">
                    <img src="img/offices_map.png" alt="Office locations"/>
                </li>
              
            </ul>
        </div>
        <div class="span12">
            <div class="separator"><hr/></div>
        </div>
        <div class="span1 copyright">
            <p>QUADRAMMA © 2014 </p>
        </div>
    </div>
    
</div>