<div id="work" class="section container-fluid" ng-controller="QWorkController">
    
    
    <div ng-repeat="slide in workSlides" id="workModal-{{slide.name}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        </div>
        <div class="modal-body">
            <div class="qjslider">
                <ul class="work-slide-ul" id="work-slide-{{slide.name}}">    
                    <li ng-repeat="path in slide.items">
                        <img ng-src="{{path}}" class="img-responsive" />
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal-footer">
            <div class="icon"><img ng-src="{{slide.icon}}"/></div>
            <div class="text color digital">{{slide.description}}</div>
            <div class="text datos">{{slide.datos}}</div>
        </div>
    </div>



    <div class="section row-fluid">
        <div class="span12">
            
                <div class="section title">
                    Discover our work
                </div>

                <div class="work-big">
                    <ul>
                        <li ng-click="modal('axn')">
                            <div class="logo280x540 img-axn"></div>
                            <div class="description-group">
                                <p class="title-digital">AXN</p>
                                <p class="subtitle">iPad game</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('lego')">
                            <div class="logo280x540 img-lego"></div>
                            <div class="description-group">
                                <p class="title-digital">Lego</p>
                                <p class="subtitle">Minifigs iPad & iPhone game</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('starbucks')">
                            <div class="logo280x540 img-starbucks"></div>
                            <div class="description-group">
                                <p class="title-digital">Starbucks</p>
                                <p class="subtitle">Christmas Mobile App</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('tntoscars')">
                            <div class="logo280x540 img-tntoscars"></div>
                            <div class="description-group">
                                <p class="title-digital">TNT Oscars</p>
                                <p class="subtitle">TNT iPad game</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                </div>
         
                
                <p class="title dos"
                >More work</p>
                <div class="work more">
                    <ul>
                        <li ng-click="modal('dianavinoli')">
                            <div class="logo280x220 img-dianavinoli"></div>
                            <div class="description-group">
                                <p class="title-digital">Diana Vi&ntilde;oli</p>
                                <p class="subtitle">Web Design</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('misspies')">
                            <div class="logo280x220 img-misspies"></div>
                            <div class="description-group">
                                <p class="title-digital">Miss Pies</p>
                                <p class="subtitle">Brand & Campaign AW14</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('jazmin')">
                            <div class="logo280x220 img-jazmin"></div>
                            <div class="description-group">
                                <p class="title-digital">Jazmin Chebar</p>
                                <p class="subtitle">Ipad & Iphone App</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('rafaelvinoli')">
                            <div class="logo280x220 img-rafaelvinoli"></div>
                            <div class="description-group">
                                <p class="title-digital">Rafael Vi&ntilde;oli</p>
                                <p class="subtitle">iPad Responsive site</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <!--
                        <li ng-click="modal('artplacer')">
                            <div class="logo280x220 img-artplacer"></div>
                            <div class="description-group">
                                <p class="title-digital">Artplacer</p>
                                <p class="subtitle">Branding & Website Design</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('orbians')">
                            <div class="logo280x220 img-orbians"></div>
                            <div class="description-group">
                                <p class="title-digital">Orbians</p>
                                <p class="subtitle">Toy brand development</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('kgd')">
                            <div class="logo280x220 img-kgd"></div>
                            <div class="description-group">
                                <p class="title-digital">KGD</p>
                                <p class="subtitle">iPad website</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('cosmeticshield')">
                            <div class="logo280x220 img-cosmeticshield"></div>
                            <div class="description-group">
                                <p class="title-digital">Cosmetic Shield</p>
                                <p class="subtitle">Web design</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('personal')">
                            <div class="logo280x220 img-personal"></div>
                            <div class="description-group">
                                <p class="title-digital">Personal</p>
                                <p class="subtitle">Personal games expo</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('bizonty')">
                            <div class="logo280x220 img-bizonty"></div>
                            <div class="description-group">
                                <p class="title-digital">Bizonti</p>
                                <p class="subtitle">Branding development</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('tntplaytime')">
                            <div class="logo280x220 img-tntplaytime"></div>
                            <div class="description-group">
                                <p class="title-digital">TNT Playtime</p>
                                <p class="subtitle">iPad game</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        <li ng-click="modal('esfera')">
                            <div class="logo280x220 img-esfera"></div>
                            <div class="description-group">
                                <p class="title-digital">Esfera</p>
                                <p class="subtitle">Digital book</p>
                            </div>
                            <div class="line-group"><div class="line-hide"></div><div class="line-digital"></div></div>
                        </li>
                        -->
                    </ul>
                </div>
        </div>
    </div>
</div>